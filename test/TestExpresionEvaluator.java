
import expresionevaluator.Addition;
import expresionevaluator.Constant;
import expresionevaluator.Substraction;
import org.junit.Assert;
import org.junit.Test;

public class TestExpresionEvaluator {

    @Test
    public void constantTest() {
        Assert.assertEquals(1, new Constant(1).calculate());
        Assert.assertEquals(7, new Constant(7).calculate());
    }

    @Test
    public void operationTest() {
        Assert.assertEquals(3, new Addition(new Constant(1).getValue(), new Constant(2).getValue()).calculate());
        Assert.assertEquals(7, new Addition(new Constant(5).getValue(), new Constant(2).getValue()).calculate());
        Assert.assertEquals(1, new Substraction(new Constant(2).getValue(), new Constant(1).getValue()).calculate());
        Assert.assertEquals(0, new Substraction(new Constant(2).getValue(), new Constant(2).getValue()).calculate());
        Assert.assertEquals(-1, new Substraction(new Constant(2).getValue(), new Constant(3).getValue()).calculate());
       
        Assert.assertEquals(-1, new Substraction(new Constant(2.0).getValue(), new Constant(3).getValue()).calculate());
        Assert.assertEquals(3, new Addition(new Constant(1.0).getValue(), new Constant(2.0).getValue()).calculate());
    }
}