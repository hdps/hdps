package expresionevaluator;

public class Constant<Type> implements Expression{
    
    private Type value;

    public Constant(Type value){
        this.value = value;
    }

    public Type Constant(){
        return this.value;
    }
    
    
    @Override
    public Type calculate() {
        return value;
    }
    
    public Type getValue(){
        return value;
    }
}