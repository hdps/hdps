package expresionevaluator;

public interface Expression<Type>{
    
    Type calculate();

}
