package expresionevaluator;

public class Addition<Type> extends Operation{

    public Addition(Type left, Type right) {
        super(left, right);
    }

    @Override
    public Type calculate() {    
        return this.left + this.right;
    }
}
