package expresionevaluator;

public abstract class Operation<Type> implements Expression{
    
    protected final Type left;
    protected final Type right;
    
    public Operation(Type left, Type right){
    
        this.left=left;
        this.right=right;        
    }
    
}
