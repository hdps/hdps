package expresionevaluator;

public class Substraction<Type> extends Operation {

    public Substraction (Type left, Type right){
        super(left, right);
    }

    @Override
    public Type calculate() {
        return this.left- this.right;
    }
    
}
